import React, { useState, createContext } from 'react'
import { View, FlatList, Text, StyleSheet } from 'react-native';

export const RootContext = createContext();

const ConsumerView = () => (
    <RootContext.Consumer>
        {value => {
            return <View>
                <FlatList data={value.name} renderItem={ ({item,index}) => (<View key={index} style={styles.container}><Text style={{fontWeight: 'bold'}}>{item.name}</Text><Text>{item.position}</Text></View>) } />
            </View>
        }}
    </RootContext.Consumer>
)

const ContextAPI = () => {

    const [ name, setName ] = useState([
        {
            name: 'Zakky Muhammad Fajar',
            position: 'Trainer 1 React Native Lanjutan'
        },
        {
            name: 'Mukhlis Hanafi',
            position: 'Trainer 2 React Native Lanjutan'
        }
    ])

    return (
        <RootContext.Provider
            value={{
                name
            }}
        >
            <ConsumerView/>
        </RootContext.Provider>
    )
}

const styles = StyleSheet.create({
    container: {
        borderWidth: 1,
        borderColor: 'black',
        margin: 10,
        padding: 10,
    }
})

export default ContextAPI;
