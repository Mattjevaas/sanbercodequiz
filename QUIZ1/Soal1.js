import React, { Component, useState, useEffect } from 'react'
import { Text, View } from 'react-native'

function FComponent(){

    const [name,setName] = useState("Jhon Doe")

    useEffect(()=>{
        const timer = setTimeout(() => {
            setName("Asep");
        }, 3000);
        return () => clearTimeout(timer);
    },[name])

    return(
        <View>
            <Text>{name}</Text>
        </View>
    )
}

export default FComponent