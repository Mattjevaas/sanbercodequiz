/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
} from 'react-native';
import Soalone from './QUIZ1/Soal1'
import Soaltwo from './QUIZ1/Soal2'

function App() {

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        {/* <Soalone/> */}
        <Soaltwo/>
      </SafeAreaView>
    </>
  );
  
};

const styles = StyleSheet.create({
  
});

export default App;
